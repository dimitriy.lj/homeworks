const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        tabs.forEach(tab => tab.classList.remove('active'));
        tab.classList.add('active');
        let selectedTab = tab.dataset.tab;
        tabContents.forEach(content => {
            content.style.display = 'none';
        });
        let selectedContent = document.querySelector(`.tabs-content li[data-tab="${selectedTab}"]`);
        selectedContent.style.display = 'block';
    });
});

/*
P.S. Я так і не поборов зміщення контенту після натискання будь-якого елементу списку =(
    Якщо я давав марджин (знаю що так собі варіант) - він діяв і після задіяння js.
    Position - спрацьовує тільки для початкових позицій елементів (або я використовува не корктно)
    Спробував поміщати <script> в різні місця html - також не працює (думав що по різному відпрацьовує в різних місцях. Так, по різному, але не так як потрібно xD)
    Будь ласка, підкажіть яким способом цього можна досягти.
*/