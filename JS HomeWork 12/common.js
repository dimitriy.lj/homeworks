const buttons = document.querySelectorAll(".btn");

function highlightButton(event) {
    const key = event.key.toUpperCase();
    const keyCode = event.keyCode
    buttons.forEach(button => {
        const buttonKey = button.getAttribute("data-key");
        if (buttonKey === key || (buttonKey === "Enter" && keyCode === 13)) {
            button.classList.add("active");
        } else {
            button.classList.remove("active");
        }
    });
}

document.body.addEventListener("keydown", event => {
    if (event.key === "Enter") {
        event.preventDefault();
    }
    highlightButton(event);
});

/*
    1. Чому для роботи з input не рекомендується використовувати клавіатуру?
Це все зв'язано з локалізацією. Тобто в різних країнах чи на різних мовах (розкладках клавіатури) будуть різні значення кнопок.
*/