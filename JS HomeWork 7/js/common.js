/*Завдання
Реалізувати функцію фільтру масиву за вказаним типом даних. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент – масив, який міститиме будь-які дані, другий аргумент – тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив [‘hello’, ‘world’, 23, ’23’, null], і другим аргументом передати ‘string’, то функція поверне масив [23, null].
*/
function filter(array, type) {
    return array.filter(function (item) {
        return typeof item !== type;
    });
}

let arr = ['Darth', 'Vader', 300, '300', null, '300', undefined, 'programmer'];
let filteredArray = filter(arr, 'string');

console.log(filteredArray); //300, null, undefined

/*
    1. Опишіть своїми словами як працює метод forEach.
Метод forEach це вбудований метод масиву, який виконує задану функцію для кожного елемента масиву, дозволяючи пройти через масив і виконати операції для кожного елемента окремо.
Самий простий приклад:
let numbers = [1, 3, 7, 33, 300];
numbers.forEach(function(number) {
  console.log(number);
});

    2.  Як очистити масив?
1) Ми можемо задати нульову довжину масиву через властивість length:
let array = [1, 3, 7, 33, 300];
array.length = 0;
2) Методом splice() ми можемо очистити як весь масив, так і точково:
let array = [1, 3, 7, 33, 300];
array.splice(0, array.length);
3) Втупу присвоїти нульове значення:
let array = [1, 3, 7, 33, 300];
array = [];

    3. Як можна перевірити, що та чи інша змінна є масивом?
1) Можна скористатися оператором instanceof:
let array = [1, 3, 7, 33, 300];
let isArr = array instanceof Array;
console.log(isArr);
2) Ще є оператор Array.isArray():
let arr = [1, 3, 7, 33, 300];
let isArr = Array.isArray(arr);
console.log(isArr);
*/