/* 
Знайти всі параграфи на сторінці та встановити колір фону #ff0000
Знайти елемент із id=”optionsList”. Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
Встановіть в якості контента елемента з класом testParagraph наступний параграф – This is a paragraph
Отримати елементи
, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
*/

let findP = document.getElementsByTagName('p')
Array.from(findP).forEach((element) => {
    element.style.backgroundColor = '#ff0000';
});
let findId = document.getElementById("optionsList")
console.log(findId);
// console.log("Parent element:", findId.parentElement);
console.log("Parent element:", findId.parentNode);
let children = findId.childNodes;
children.forEach((child) => {
    console.log("Name: " + child.nodeName + ", node type: " + child.nodeType);
});
document.getElementById("testParagraph").innerText = "This is a paragraph"

let newClass = document.querySelector(".main-header");
console.log(newClass);
let newClassElements = newClass.children;
console.log(newClassElements);

Array.from(newClassElements).forEach((element) => {
    element.classList.add("nav-item");
    console.log(element);
});

let elements = document.querySelectorAll(".section-title");

elements.forEach((element) => {
    element.classList.remove("section-title");
});

/* 
    1.Опишіть своїми словами що таке Document Object Model (DOM)
DOM (Document Object Model) - це представлення веб-сторінки або XML-документа у вигляді деревоподібної структури, де кожен елемент, текст або атрибут представлені як об'єкти. Ми можемо отримати доступ до елементів документа і взаємодіяти з ними, змінюючи їх властивості, додавати нові елементи, видаляти або змінювати все, куди руки дотягнуться.
   
    2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
Різниця в тому, що innerHTML працює з HTML-розміткою всередині елемента. innerText працює тільки з видимим текстом, виключаючи HTML-розмітку.
Простий приклад:
<div id="myElement">
  <strong>Darth</strong> <em>Vader</em>
</div>
const element = document.getElementById('myElement');
console.log(element.innerHTML); в результаті буде: <strong>Darth</strong> <em>Vader</em>
console.log(element.innerText); в результаті буде: Darth Vader

    3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
Один з найпоширеніших і самих "чьотких"(тому що метод швидкий і ефективний як mr.Proper xD) способів звернення - це використання методу getElementById. Цей метод шукає елемент за його унікальним ідентифікатором (ID).
Приклад навів вище, в другому теоретичному питанні.
Але, якщо потрібно звернутися до багатьох елементів або шукати їх за класом, тегом або іншими властивостями, тоді розглянути інші методи: getElementsByClassName, getElementsByTagName або querySelectorAll.
Приклад явно використаний код в кінці практичного заняття:
let elements = document.querySelectorAll(".section-title");
Просто,легко і без напрягу зібрав всі потрібні класи.
На мою думку, найкращий спосіб звернення по id. Але так як він має бути унікальним - його не можна використати масово.
    */