const images = document.querySelectorAll(".image-to-show");
let currentIndex = 0;
let intervalId;

function showImage(index) {
    images.forEach((image, i) => {
        if (i === index) {
            image.style.display = "block";
        } else {
            image.style.display = "none";
        }
    });
}

function startSlideshow() {
    if (!intervalId) {
        showImage(currentIndex);
        intervalId = setInterval(() => {
            currentIndex = (currentIndex + 1) % images.length;
            showImage(currentIndex);
        }, 3000);
    }
}

function stopSlideshow() {
    clearInterval(intervalId);
    intervalId = null;
}

function resumeSlideshow() {
    if (!intervalId) {
        intervalId = setInterval(() => {
            currentIndex = (currentIndex + 1) % images.length;
            showImage(currentIndex);
        }, 3000);
    }
}

/*
    1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
Відмінність полягає в тому, що setTimeout() виконує функцію лише один раз після зазначеного інтервалу, а setInterval() виконує функцію регулярно з періодичністю заданою інтервалом, поки його хтось не зупинить))

    2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Коли в функцію setTimeout() передається нульова затримка, вона спрацює миттєво. Але насправді, вона не виконається миттєво, а після того, як будуть оброблені інші вже заплановані операції та функції в потоці виконання. Тому, хоча затримка теоретично нульова, фактично є деякий мінімальний час, який необхідний для виконання інших операцій перед початком виконання функції з нульовою затримкою.

    3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Функція clearInterval() зупиняє циклічний виклик функції, який був встановлений з допомогою setInterval(). Якщо не викликати clearInterval(), цикл буде продовжуватись і забирати ресурси системи, навіть коли вже не потрібен повторюваний виклик функції.
*/