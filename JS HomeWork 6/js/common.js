/* Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
Технічні вимоги:
Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з’єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об’єкта. */

function createNewUser() {
  let firstName = prompt("Введіть ім'я:");
  let lastName = prompt("Введіть прізвище:");
  let brthd = prompt("Введіть свою дату народження у форматі dd.mm.yyyy:");
  let age;
  let brthData = parseDate(brthd);

  let newUser = {
    _firstName: firstName,
    _lastName: lastName,
    _brthd: brthData,

    getLogin: function () {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },
    getAge: function () {
      let todayData = new Date();
      age = todayData.getFullYear() - this._brthd.getFullYear();
      if (
        todayData.getMonth() < this._brthd.getMonth() ||
        (todayData.getMonth() === this._brthd.getMonth() && todayData.getDate() < this._brthd.getDate())
      ) {
        age--;
      }
      return age;
    },
    getPassword: function () {
      let year = String(this._brthd.getFullYear());
      return (this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + year);
    },
    setFirstName: function (newFirstName) {
      this._firstName = newFirstName;
      return this._firstName;
    },
    setLastName: function (newLastName) {
      this._lastName = newLastName;
      return this._lastName;
    }
  };

  Object.defineProperties(newUser, {
    firstName: {
      configurable: true,
      get: function () {
        return this._firstName;
      }
    },
    lastName: {
      configurable: true,
      get: function () {
        return this._lastName;
      }
    }
  });
  return newUser;
}

function parseDate(dateString) {
  let [day, month, year] = dateString.split(".");
  return new Date(year, month - 1, day);
}

const user = createNewUser();
console.log("user login: " + user.getLogin() + `\n` + "user age: " + user.getAge() + `\n` + "user password: " + user.getPassword());



/*  
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
Екранування - це механізм, який дозволяє вставляти спеціальні символи таким чином, щоб ці ж символи не брали участь в фунціональності коду. Зазвичай для цього використовуються лапки на кнопці ~. Якщо я хочу вивести " щоб вони не брали участь в коді, я ці лапки огорну в `"`. Так працює екранування, виведеться тільки одинарні " і не буде помилки, що я їх не закрив, це буде як свичайний string. Ще один приклад, я використав в завданні вище в останньому рядку `\n` щоб \ не фіксувався js як слеш.
2. Які засоби оголошення функцій ви знаєте?
Всього є п'ять способів.
    1) Класика:
    function threeHundred() {
    // тіло функції
    }
    2) Вираз:
    const threeHundred = function() {
     // тіло функції
    };
    3) Стрілкова:
    const threeHundred = () => {
     // тіло функції
    };
    4) Метод об'єкту:
    const obj = {
    threeHundred: function() {
     // тіло функції
      }
    };
    5) Функція-конструктор (не шарю за нього ні разу =)):
    function User(name) {
    this.name = name;
    }
    const user = new User('Darth Vader');
3. Що таке hoisting, як він працює для змінних та функцій?
Hoisting в js означає, що оголошення змінних і функцій піднімаються вгору у своїх областях видимості перед виконанням коду. Це дозволяє використовувати їх перед фактичним оголошенням у коді. Змінні піднімаються, але без значень, тоді як функції піднімаються з усім своїм вмістом.
Приклади:

console.log(max); // undefined
let max = 300;
console.log(max); // 300

lev(); // виклик функції до оголошення
function lev() {
  console.log('300');
}
lev();
*/