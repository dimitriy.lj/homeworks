//services-nav
document.addEventListener("DOMContentLoaded", function () {
    const links = document.querySelectorAll(".services-link");
    const images = document.querySelectorAll(".services-image");
    const texts = document.querySelectorAll(".services-text-title");

    function setActiveTab(tab) {
        links.forEach(link => link.classList.remove("services-link-active"));
        tab.classList.add("services-link-active");
    }

    function showContent(tab) {
        images.forEach(image => image.style.display = "none");
        texts.forEach(text => text.style.display = "none");

        const imageToShow = document.querySelector(`.services-image[data-tab="${tab}"]`);
        const textToShow = document.querySelector(`.services-text-title[data-tab="${tab}"]`);

        if (imageToShow && textToShow) {
            imageToShow.style.display = "block";
            textToShow.style.display = "block";
        }
    }

    links.forEach(link => {
        link.addEventListener("click", function (event) {
            event.preventDefault();
            const tab = this.getAttribute("data-tab");
            setActiveTab(this);
            showContent(tab);
        });
    });

    const initialActiveLink = document.querySelector(".services-link-active");
    if (initialActiveLink) {
        const initialTab = initialActiveLink.getAttribute("data-tab");
        showContent(initialTab);
    }
});

//examples-nav
document.addEventListener("DOMContentLoaded", function () {
    const images = document.querySelectorAll(".examples-image");
    const loadMoreButton = document.querySelector(".examples-btn");
    const initialImagesCount = 16;
    let visibleImages = initialImagesCount;
    let currentCategory = null;

    function toggleImages() {
        images.forEach(function (image, index) {
            if (!currentCategory || image.getAttribute("data-tab") === currentCategory) {
                if (index < visibleImages) {
                    image.classList.remove("hidden");
                } else {
                    image.classList.add("hidden");
                }
            } else {
                image.classList.add("hidden");
            }
        });
    }

    loadMoreButton.addEventListener("click", function (event) {
        event.preventDefault();

        visibleImages += initialImagesCount;

        if (visibleImages >= images.length) {
            loadMoreButton.style.display = "none";
        }

        toggleImages();
    });

    const navLinks = document.querySelectorAll(".examples-link");

    navLinks.forEach(function (navLink) {
        navLink.addEventListener("click", function (event) {
            event.preventDefault();

            const tabName = navLink.getAttribute("data-tab");

            currentCategory = tabName;
            visibleImages = initialImagesCount;
            loadMoreButton.style.display = "block";

            toggleImages();
        });
    });

    toggleImages();
});

//about-nav
document.addEventListener('DOMContentLoaded', function () {
    const photos = document.querySelectorAll('.about-nav-photo');
    let activePhoto = document.querySelector('.about-photo-active');
    const personName = document.querySelector('.about-person-name');
    const personPosition = document.querySelector('.about-person-position');
    const mainPhoto = document.querySelector('.about-person-photo');

    photos.forEach((photo) => {
        photo.addEventListener('click', (event) => {
            activePhoto.classList.remove('about-photo-active');

            event.target.classList.add('about-photo-active');

            activePhoto = event.target;

            const personTab = event.target.getAttribute('data-tab');
            const newName = document.querySelector(`[data-tab="${personTab}"].about-person-name`);
            const newPosition = document.querySelector(`[data-tab="${personTab}"].about-person-position`);

            personName.textContent = newName.textContent;
            personPosition.textContent = newPosition.textContent;

            const newMainPhotoSrc = event.target.getAttribute('src');
            mainPhoto.style.transform = 'scale(0)';
            setTimeout(() => {
                mainPhoto.setAttribute('src', newMainPhotoSrc);
                mainPhoto.style.transform = 'scale(1)';
            }, 300);
        });
    });

    const leftButton = document.querySelector('.left-button');
    const rightButton = document.querySelector('.right-button');

    leftButton.addEventListener('click', () => {
        const activeIndex = Array.from(photos).findIndex((photo) => photo.classList.contains('about-photo-active'));
        const newIndex = (activeIndex - 1 + photos.length) % photos.length;

        activePhoto.classList.remove('about-photo-active');
        photos[newIndex].classList.add('about-photo-active');
        activePhoto = photos[newIndex];

        const personTab = photos[newIndex].getAttribute('data-tab');
        const newName = document.querySelector(`[data-tab="${personTab}"].about-person-name`);
        const newPosition = document.querySelector(`[data-tab="${personTab}"].about-person-position`);
        personName.textContent = newName.textContent;
        personPosition.textContent = newPosition.textContent;

        const newMainPhotoSrc = photos[newIndex].getAttribute('src');
        mainPhoto.style.transform = 'scale(0)';
        setTimeout(() => {
            mainPhoto.setAttribute('src', newMainPhotoSrc);
            mainPhoto.style.transform = 'scale(1)';
        }, 300);
    });

    rightButton.addEventListener('click', () => {
        const activeIndex = Array.from(photos).findIndex((photo) => photo.classList.contains('about-photo-active'));
        const newIndex = (activeIndex + 1) % photos.length;

        activePhoto.classList.remove('about-photo-active');
        photos[newIndex].classList.add('about-photo-active');
        activePhoto = photos[newIndex];

        const personTab = photos[newIndex].getAttribute('data-tab');
        const newName = document.querySelector(`[data-tab="${personTab}"].about-person-name`);
        const newPosition = document.querySelector(`[data-tab="${personTab}"].about-person-position`);
        personName.textContent = newName.textContent;
        personPosition.textContent = newPosition.textContent;

        const newMainPhotoSrc = photos[newIndex].getAttribute('src');
        mainPhoto.style.transform = 'scale(0)';
        setTimeout(() => {
            mainPhoto.setAttribute('src', newMainPhotoSrc);
            mainPhoto.style.transform = 'scale(1)';
        }, 300);
    });
});