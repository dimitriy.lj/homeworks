/*Завдання
Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent – DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];
Можна взяти будь-який інший масив.
Необов’язкове завдання підвищеної складності:
Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву: ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]; Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки. 

P.S. я трішки підгорів на вкладених масивах, тому все що з ними зв'язано - видалив))
*/
function myList(array, parent = document.body) {
  if (array.length === 0) {
    alert('Введіть елементи масиву!');
    return; 
  }
  const ul = document.createElement('ul');
  array.forEach(item => {
    const li = document.createElement('li');
    li.textContent = item;
    ul.appendChild(li);
  });
  parent.appendChild(ul);
  let count = 3;
  const countdownTimer = setInterval(() => {
    count--;
    if (count >= 0) {
      console.log(count);
    } else {
      clearInterval(countdownTimer);
      parent.innerHTML = '';
    }
  }, 1000);
}
const userInput = prompt('Введіть елементи масиву через кому:');
const array = userInput.split(',').map(item => item.trim());
let parentSelector = prompt('Введіть селектор батьківського елемента (необов\'язково):', 'document.body');
let parentElement;
if (parentSelector === null) {
  alert('Добрий день');
} else if (parentSelector === 'document.body') {
  parentElement = document.body;
} else {
  parentElement = document.querySelector(parentSelector);
}
myList(array, parentElement);

/*
  1.Опишіть, як можна створити новий HTML тег на сторінці.
Найпростіший спосіб це метод document.createElement('newTag')
Трішки кращий спосіб це створити елемент з уточненням куди його помістити. parentElement.appendChild(newTag), де для parentElement потрібно буде вказати місце в DOM деревці.
В своєму коду я використав parent.appendChild(ul), де попередньо для батьківського елемента ul вказав місце в документі const ul = document.createElement('ul').

  2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр функції insertAdjacentHTML визначає позицію, на якій буде вставлено HTML-рядок. Є чотири варінти:
'beforebegin': Вставляє HTML-рядок перед елементом.
'afterbegin': Вставляє HTML-рядок як першу дочірню ноду елемента.
'beforeend': Вставляє HTML-рядок як останню дочірню ноду елемента.
'afterend': Вставляє HTML-рядок після елемента.

  3. Як можна видалити елемент зі сторінки?
Можна використати метод remove().
const element = document.querySelector('.zagolovok');
element.remove();
*/