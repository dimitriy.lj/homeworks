/*1. Отримати за допомогою модального вікна браузера число, яке введе користувач.
Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає – вивести в консоль фразу “Sorry, no numbers”
Обов’язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран, доки не буде введено ціле число.

2. Отримати два числа, m і n. Вивести в консоль усі прості числа в діапазоні від m до n (менше із введених чисел буде m, більше буде n). Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище, вивести повідомлення про помилку і запитати обидва числа знову.*/

// 1.

// let userNumber = +prompt("Give me any number");
// while (isNaN(userNumber)) {
//   userNumber = +prompt("Allow, vvedy chyslo!");
//   while (userNumber % 1 !== 0) {
//     userNumber = +prompt("Allow, vvedy cile chyslo");
//   }
// }
// let foundNumbers = false;
// for (let i = 5; i <= userNumber; i++) {
//   if (i % 5 === 0) {
//     console.log(i);
//     foundNumbers = true;
//   }
// }
// if (!foundNumbers) {
//   console.log("Sorry, no numbers");
// }

// 2.

// let m = +prompt("Vvedy pershe chyslo");
// let n = +prompt("Vvedy druge chyslo");
// while (isNaN(m) || isNaN(n)) {
//   alert("Fatal error, Vvedy chyslo, kurva");
//   m = +prompt("Vvedy pershe chyslo, kurva");
//   n = +prompt("Vvedy druge chyslo, kurva");
// }
// while (m >= n) {
//   alert("Fatal error, vvedy pershe chyslo menwe, nizh druge");
//   m = +prompt("Vvedy pershe chyslo, kurva");
//   n = +prompt("Vvedy druge chyslo, kurva");
// }
// let foundNumbers = false;
// for (let i = m; i <= n; i++) {
//   let simpleNumber = true;

//   if (i < 2) {
//     simpleNumber = false;
//   } else {
//     for (let s = 2; s < i; s++) {
//       if (i % s === 0) {
//         simpleNumber = false;
//       }
//     }
//   }
//   if (simpleNumber) {
//     console.log(i);
//     foundNumbers = true;
//   }
// }
// if (!foundNumbers) {
//   console.log("Sorry, Michael");
// }

// 2. (Друга розв'язка, можливо я не так зрозумів умову, що m < n. В цьому варіанті я не буду перепитувати користувача про менше і більше число, а сам присвою значення)

// let firstNum = +prompt("Vvedy pershe chyslo");
// let secondNum = +prompt("Vvedy druge chyslo (ne rivne perwomu)");
// while (isNaN(firstNum) || isNaN(secondNum) || firstNum === secondNum) {
//   alert("Fatal error, Vvedy chyslo, kurva");
//   firstNum = +prompt("Vvedy pershe chyslo, kurva");
//   secondNum = +prompt("Vvedy druge chyslo, kurva (ne rivne perwomu)");
// }
// let m, n;
// if (firstNum < secondNum) {
//   m = firstNum;
//   n = secondNum;
// } else {
//   m = secondNum;
//   n = firstNum;
// }
// let foundNumbers = false;
// for (let i = m; i <= n; i++) {
//   let simpleNumber = true;
//   if (i < 2) {
//     simpleNumber = false;
//   } else {
//     for (let s = 2; s < i; s++) {
//       if (i % s === 0) {
//         simpleNumber = false;
//       }
//     }
//   }
//   if (simpleNumber) {
//     console.log(i);
//     foundNumbers = true;
//   }
// }
// if (!foundNumbers) {
//   console.log("Sorry, Michael");
// }

/* 1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
Цикли потрібні для досягнення результату при певних умовах. Ми задаємо умови, і за допомогою циклів, кількість ітерацій допоможе реалізувати рішення.
В наступному питанні більш точніше розкриваєтсья їхнє використання.
   2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
В ситуації, коли потрібно щоб користувач ввів число, я використав цикл while (будемо тупити доти, поки не введеш коректні даніxD), щоб він скільки б разів не намагався ввести текст чи символи - програма не запрацювала. Цикл допоміг добитися бажаного результату, незалежно від того скільки разів користувач тупив.
Ще використовував цикл for в ситуації, коли мені потрібно було відібрати прості числа. Це дещо складніший цикл ніж while, так як мені потрібно було уточнити більше параметрів, а саме початкове значення, умову і покроковість.
   3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
Явне перетворення - це коли розробник пише кодом: "я хочу ось в цьому місці змінити тип даних".
Пр: let n = "300"
    let n = +n
    console.log(n)  в цьому випадку розробник явно натякає, що тип змінної в кінцевому результаті буде число
Неявне приведення виходить "само собою =)", або в автоматичному режимі (коли це можливо)
Пр: let m = 300
    let str = "traktorist is: " + m
    console.log(str)    в цьому випадку розробник явно не натякав на перетворення типів, але все ж таки це відбулося.
    Як результат в консолі виведеться значення "traktorist is: 300", тип даних буде string, так як приведення працює (знову ж таки, коли це можливо) на другий оперант.
*/
